import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Delete,
  Patch,
  Query,
  UseGuards,
  Logger,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from '../auth/get-user.decorator';
import { User } from '../auth/user.entity';
import { CreateTaskDto } from './dto/create-task.dto';
import { FilterTaskDto } from './dto/filter-task.dto';
import { UpdateTaskStatusDto } from './dto/update-task-status.dto';
import { Task } from './task.entity';

import { TasksService } from './tasks.service';

@Controller('tasks')
@UseGuards(AuthGuard())
export class TasksController {
  private logger = new Logger('TasksController');
  constructor(private tasksService: TasksService) {}

  @Get()
  getTasks(
    @Query() filterDto: FilterTaskDto,
    @GetUser() user: User,
  ): Promise<Task[]> {
    this.logger.verbose(
      `User ${user.username} retrieving all tasks. Filter ${JSON.stringify(
        filterDto,
      )}`,
    );
    return this.tasksService.getTasks(filterDto, user);
  }

  @Get('/:id')
  getTask(@Param('id') id: number, @GetUser() user: User): Promise<Task> {
    return this.tasksService.findTaskById(id, user);
  }

  @Post()
  createNewTask(
    @Body() createTaskDto: CreateTaskDto,
    @GetUser() user: User,
  ): Promise<Task> {
    try {
      const newTask = this.tasksService.createTask(createTaskDto, user);
      return newTask;
    } catch (error) {
      throw error;
    }
  }

  @Patch('/:id/status')
  updateTaskStatus(
    @Param('id') id: number,
    @Body() updateTaskStatusDto: UpdateTaskStatusDto,
    @GetUser() user: User,
  ): Promise<Task> {
    try {
      const { status } = updateTaskStatusDto;
      const updatedTask = this.tasksService.updateTask(id, status, user);
      return updatedTask;
    } catch (error) {
      throw error;
    }
  }

  @Delete('/:id')
  deleteExistingTask(
    @Param('id') id: number,
    @GetUser() user: User,
  ): Promise<void> {
    try {
      const task = this.tasksService.deleteTask(id, user);
      return task;
    } catch (error) {
      throw error;
    }
  }
}
